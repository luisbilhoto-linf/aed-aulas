import java.util.Scanner;
//import java.util.Vector;

public class Main {
	private static Scanner input = new Scanner(System.in);
//	private static Vector<Carta> cartas = new Vector<Carta>();
	private static Vector cartas = new Vector();

	/**
	 * printWait
	 * 
	 * Este m�todo imprime uma mensagem de espera e sucessivamente espera pelo input
	 * de qualquer coisa. Funcionalidade de embelezamento e melhorias de
	 * usabilidade.
	 * 
	 * @param input Scanner declarado em main;
	 */
	private static void printWait() {
		System.out.println("\nPressione a tecla ENTER para continuar...");
		input.nextLine(); // Wait until user press ENTER key.
		input.nextLine(); // Wait until user press ENTER key.
	}

	public static void main(String[] args) {

		int menu = 0;
		do {
			System.out.println("MENU PRINCIPAL");
			System.out.println();
			System.out.println("1. Adicionar cartas");
			System.out.println("2. Imprimir lista");
			System.out.println("3. Imprimir uma carta");
			System.out.println("4. Remover carta por �ndice");
			System.out.println("0. Sair");

			menu = input.nextInt();
			switch (menu) {
			case 1:
				System.out.println("ADICIONAR CARTAS");
				for (int i = 0; i < 10; i++) {
					cartas.adicionar(new Carta());
				}
				printWait();
				break;

			case 2:
				System.out.println("IMPRIMIR LISTA");
				for (int i = 0; i < cartas.getTamanho(); i++) {
					System.out.println(cartas.get(i).toString());
				}
				printWait();
				break;

			case 3:
				System.out.println("IMPRIMIR UMA CARTA");
				System.out.print("�ndice: ");
				System.out.println(cartas.get(input.nextInt()).toString());
				printWait();
				break;

			case 4:
				System.out.println("REMOVER CARTA POR �NDICE");
				System.out.print("�ndice: ");
				cartas.retirar(input.nextInt());
				printWait();
				break;

			case 0:
				break;

			default:
				System.out.printf("Op��o '%s' n�o reconhecida! Tente novamente.\n", menu == 13 ? "ENTER" : menu);
				break;
			}
		} while (menu != 0);
		input.close();
	}
}
