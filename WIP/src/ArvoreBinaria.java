public class ArvoreBinaria {
	private No raiz;
	private int nElementos;

	public ArvoreBinaria() {
		nElementos = 0;
		raiz = null;
	}

	private class No {
		String oElemento;
		No esq;
		No dir;
		No pai;

		No(String novo) {
			esq = null;
			dir = null;
			pai = null;
			oElemento = novo;
		}
	}

	public void inserir(String novo) {
		No novoNo = procuraNo(novo);
		novoNo = new No(novo);
		No anterior = null;
		No actual = raiz;
		while (actual != null) {
			anterior = actual;
			if (actual.oElemento.compareTo(novo) > 0)
				actual = actual.dir;
			else
				actual = actual.esq;
		}
		if (raiz == null)
			raiz = novoNo;
		else if (anterior.oElemento.compareTo(novo) > 0)
			anterior.dir = novoNo;
		else
			anterior.esq = novoNo;
		novoNo.pai = anterior;
		nElementos++;
	}

	public No menor(No actual) {
		if (actual == null)
			return null;

		while (actual.esq != null)
			actual = actual.esq;

		return actual;
	}

	public No menor() {
		return menor(raiz);
	}

	public No maior(No actual) {
		if (actual == null)
			return null;

		while (actual.dir != null)
			actual = actual.dir;

		return actual;
	}

	public No maior() {
		return maior(raiz);
	}

	public boolean procura(String s) {
		return procuraNo(s) != null;
	}

	private No procuraNo(String s, No actual) {
		if (actual != null) {
			if (actual.oElemento.compareTo(s) == 0) {
				return actual;
			}
			if (actual.oElemento.compareTo(s) < 0) {
				return procuraNo(s, actual.esq);
			}
			if (actual.oElemento.compareTo(s) > 0) {
				return procuraNo(s, actual.dir);
			}
		}

		return null;
	}

	public No procuraNo(String s) {
		return procuraNo(s, raiz);
	}

	private No sucessor(No pos) {
		if (pos.dir != null)
			return menor(pos.dir);

		No actual = pos;
		No ancestral = actual.pai;
		while (ancestral != null && ancestral.dir == actual) {
			actual = ancestral;
			ancestral = ancestral.pai;
		}

		return ancestral;
	}

	public No sucessor(String valor) {
		No pos = procuraNo(valor);
		return sucessor(pos);
	}

	public No predecessor(No pos) {
		if (pos.esq != null)
			return maior(pos.esq);

		No actual = pos;
		No ancestral = actual.pai;
		while (ancestral != null && ancestral.esq == actual) {
			actual = ancestral;
			ancestral = ancestral.pai;
		}

		return ancestral;
	}

	public No predecessor(String valor) {
		No pos = procuraNo(valor);
		return predecessor(pos);
	}

	public void eliminar(String x) {
		No actual, aApagar, filho, pai;
		actual = procuraNo(x);
		if (actual.esq == null || actual.dir == null)
			aApagar = actual;
		else
			aApagar = sucessor(actual);
		if (aApagar.esq != null)
			filho = aApagar.esq;
		else
			filho = aApagar.dir;
		pai = aApagar.pai;
		if (filho != null)
			filho.pai = pai;
		if (pai == null)
			raiz = filho;
		else if (pai.esq == aApagar)
			pai.esq = filho;
		else
			pai.dir = filho;
		if (aApagar != actual)
			actual.oElemento = aApagar.oElemento;
		nElementos--;
	}

	public void listarOrdemIterativa(String s) {
		if (raiz == null)
			return;

		No proximo = menor();
		while (proximo != null) {
			if (proximo.oElemento.startsWith(s)) {
				System.out.println(proximo.oElemento);
			}
			proximo = sucessor(proximo);
		}
	}

	// Function to convert input BST
	// to right linked list
	// known as vine or backbone.
	private int bstToVine(No grand) {
		int count = 0;

		// Make tmp pointer to traverse
		// and right flatten the given BST.
		No tmp = grand.dir;

		// Traverse until tmp becomes NULL
		while (tmp != null) {

			// If left exist for node
			// pointed by tmp then
			// right rotate it.
			if (tmp.esq != null) {
				No oldTmp = tmp;
				tmp = tmp.esq;
				oldTmp.esq = tmp.dir;
				tmp.dir = oldTmp;
				grand.dir = tmp;
			}

			// If left dont exists
			// add 1 to count and
			// traverse further right to
			// flatten remaining BST.
			else {
				count++;
				grand = tmp;
				tmp = tmp.dir;
			}
		}

		return count;
	}

	// Function to compress given tree
	// with its root as grand.right.
	private void compress(No grand, int m) {

		// Make tmp pointer to traverse
		// and compress the given BST.
		No tmp = grand.dir;

		// Traverse and left-rotate root m times
		// to compress given vine form of BST.
		for (int i = 0; i < m; i++) {
			No oldTmp = tmp;
			tmp = tmp.dir;
			grand.dir = tmp;
			oldTmp.dir = tmp.esq;
			tmp.esq = oldTmp;
			grand = tmp;
			tmp = tmp.dir;
		}
	}

	// Function to calculate the
	// log base 2 of an integer
	private int log2(int N) {

		// calculate log2 N indirectly
		// using log() method
		return (int) (Math.log(N) / Math.log(2));
	}

	// Function to implement the algorithm
	public void balanceBST() {

		// create an empty dummy node
		No grand = new No("");

		// assign the right of dummy node as our input BST
		grand.dir = raiz;

		// get the number of nodes in input BST and
		// simultaneously convert it into right linked list.
		int count = bstToVine(grand);

		// gets the height of tree in which all levels
		// are completely filled.
		int h = log2(count + 1);

		// get number of nodes until second last level
		int m = (int) Math.pow(2, h) - 1;

		// Left rotate for excess nodes at last level
		compress(grand, count - m);

		// Left rotation till m becomes 0
		// Step is done as mentioned in algo to
		// make BST balanced.
		for (m = m / 2; m > 0; m /= 2) {
			compress(grand, m);
		}

		// return the balanced tree
		raiz = grand.dir;
	}
}
