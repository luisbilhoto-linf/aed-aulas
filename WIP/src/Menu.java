import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Menu {
	private static ArvoreBinaria arvore = new ArvoreBinaria();
	private static Scanner teclado = new Scanner(System.in);

	private static void leitor(String ficheiro) {
		File file = new File("./bin/textos/" + ficheiro);
		if (file.exists()) {
			try {
				Scanner sc = new Scanner(file);
				while (sc.hasNext())
					arvore.inserir(sc.next().replaceAll("[.,]", "").trim().toLowerCase());
				sc.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("> O ficheiro n�o foi encontrado!");
		}
	}

	public static void main(String[] args) {
		long t = 0;
		int opcao = 0;
		do {
			System.out.println("1 - Inserir texto a partir de um ficheiro");
			System.out.println("2 - Inserir texto a partir da consola");
			System.out.println("3 - Procurar Palavras");
			System.out.println("4 - Remover palavra");
			System.out.println("5 - Balanciar �rvore ");
			System.out.println("0 - Sair");

			System.out.println("Escolha uma op��o");
			opcao = teclado.nextInt();
			teclado.nextLine();
			switch (opcao) {
			case 1:
				System.out.print("Insira o nome do Texto: ");
				leitor(teclado.nextLine());
				System.out.println();
				break;
			case 2:
				System.out.println("Introduza o texto que pretende inserir:");
				String palavras = teclado.nextLine().trim();
				Scanner sc = new Scanner(palavras);
				while (sc.hasNext())
					arvore.inserir(sc.next().replaceAll("[.,]", "").trim().toLowerCase());
				sc.close();
				System.out.println();
				break;
			case 3:
				t = System.nanoTime();
				System.out.print("Palavra: ");
				arvore.listarOrdemIterativa(teclado.nextLine().trim().toLowerCase());
				System.out.println();
				System.out.println("Demorou " + (System.nanoTime() - t) + "ns.");
				System.out.println();
				teclado.nextLine();
				break;
			case 4:
				t = System.nanoTime();
				System.out.print("Palavra: ");
				arvore.eliminar(teclado.nextLine().toLowerCase());
				System.out.println();
				System.out.println("Demorou " + (System.nanoTime() - t) + "ns.");
				System.out.println();
				teclado.nextLine();
				break;
			case 5:
				arvore.balanceBST();
				System.out.println();
				break;
			case 0:
				System.exit(0);
			default:
				System.out.println("Escolha inv�lida. Selecione uma op��o correta.");
				System.out.println();
				teclado.nextLine();
				break;
			}
		} while (opcao != 0);
		teclado.close();
	}
}
