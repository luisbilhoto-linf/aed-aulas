public class ArvBin {
	private No raiz;
	private int nElementos;

	public ArvBin() {
		nElementos = 0;
		raiz = null;
	}

	private class No {
		String oElemento;
		int nVezes;
		No esq;
		No dir;
		No pai;

		No(String novo) {
			esq = null;
			dir = null;
			pai = null;
			oElemento = novo;
			nVezes = 1;
		}
	}

	public void inserir(String novo) {
		No novoNo = procuraNo(novo);
		if (novoNo != null)
			novoNo.nVezes++;
		else {
			novoNo = new No(novo);
			No anterior = null;
			No actual = raiz;
			while (actual != null) {
				anterior = actual;
				if (actual.oElemento.compareTo(novo) > 0)
					actual = actual.dir;
				else
					actual = actual.esq;
			}
			if (raiz == null)
				raiz = novoNo;
			else if (anterior.oElemento.compareTo(novo) > 0)
				anterior.dir = novoNo;
			else
				anterior.esq = novoNo;
			novoNo.pai = anterior;
			nElementos++;
		}
	}

	public No menor(No actual) {
		if (actual == null)
			return null;

		while (actual.esq != null)
			actual = actual.esq;

		return actual;
	}

	public No menor() {
		return menor(raiz);
	}

	public No maior(No actual) {
		if (actual == null)
			return null;

		while (actual.dir != null)
			actual = actual.dir;

		return actual;
	}

	public No maior() {
		return maior(raiz);
	}

	public boolean procura(String s) {
		return procuraNo(s) != null;
	}

	private No procuraNo(String s, No actual) {
		if (actual != null) {
			if (actual.oElemento.compareTo(s) == 0) {
				return actual;
			}
			if (actual.oElemento.compareTo(s) < 0) {
				return procuraNo(s, actual.esq);
			}
			if (actual.oElemento.compareTo(s) > 0) {
				return procuraNo(s, actual.dir);
			}
		}

		return null;
	}

	public No procuraNo(String s) {
		return procuraNo(s, raiz);
	}

	private No sucessor(No pos) {
		if (pos.dir != null)
			return menor(pos.dir);

		No actual = pos;
		No ancestral = actual.pai;
		while (ancestral != null && ancestral.dir == actual) {
			actual = ancestral;
			ancestral = ancestral.pai;
		}

		return ancestral;
	}

	public No sucessor(String valor) {
		No pos = procuraNo(valor);
		return sucessor(pos);
	}

	public No predecessor(No pos) {
		if (pos.esq != null)
			return maior(pos.esq);

		No actual = pos;
		No ancestral = actual.pai;
		while (ancestral != null && ancestral.esq == actual) {
			actual = ancestral;
			ancestral = ancestral.pai;
		}

		return ancestral;
	}

	public No predecessor(String valor) {
		No pos = procuraNo(valor);
		return predecessor(pos);
	}

	private void eliminar(String x) {
		No actual, aApagar, filho, pai;
		actual = procuraNo(x);
		if (actual.esq == null || actual.dir == null)
			aApagar = actual;
		else
			aApagar = sucessor(actual);
		if (aApagar.esq != null)
			filho = aApagar.esq;
		else
			filho = aApagar.dir;
		pai = aApagar.pai;
		if (filho != null)
			filho.pai = pai;
		if (pai == null)
			raiz = filho;
		else if (pai.esq == aApagar)
			pai.esq = filho;
		else
			pai.dir = filho;
		if (aApagar != actual)
			actual.oElemento = aApagar.oElemento;
		nElementos--;
	}

	public boolean remover(String s) {
		if (procura(s)) {
			eliminar(s);
			return true;
		}

		return false;
	}

	private String listarOrdem(No no) {
		if (no == null)
			return "";

		String string = "";
		string += listarOrdem(no.esq);
		string += String.format("> %s (%d)\r\n", no.oElemento, no.nVezes);
		string += listarOrdem(no.dir);

		return string;
	}

	public String listarOrdem() {
		return listarOrdem(raiz);
	}

	public String listarOrdemIterativa() {
		if (raiz == null)
			return "";

		String string = "";
		No proximo = menor();
		while (proximo != null) {
			string += String.format("> %s (%d)\r\n", proximo.oElemento, proximo.nVezes);
			proximo = sucessor(proximo);
		}

		return string;
	}

	public String listarPreOrdem(No no) {
		if (no == null)
			return "";

		String string = "";
		string += String.format("> %s (%d)\r\n", no.oElemento, no.nVezes);
		string += listarPreOrdem(no.esq);
		string += listarPreOrdem(no.dir);

		return string;
	}

	public String listarPosOrdem(No no) {
		if (no == null)
			return "";

		String string = "";
		string += listarPosOrdem(no.esq);
		string += listarPosOrdem(no.dir);
		string += String.format("> %s (%d)\r\n", no.oElemento, no.nVezes);

		return string;
	}

	public String listarProcura(String s) {
		if (raiz == null)
			return "";

		String string = "";
		No proximo = menor();
		while (proximo != null) {
			if (proximo.oElemento.startsWith(s)) {
				string += String.format("> %s (%d)\r\n", proximo.oElemento, proximo.nVezes);
			}
			proximo = sucessor(proximo);
		}

		return string;
	}

	public boolean estaVazia() {
		return nElementos <= 0;
	}

	// Function to convert input BST
	// to right linked list
	// known as vine or backbone.
	private int bstToVine(No grand) {
		int count = 0;

		// Make tmp pointer to traverse
		// and right flatten the given BST.
		No tmp = grand.dir;

		// Traverse until tmp becomes NULL
		while (tmp != null) {

			// If left exist for node
			// pointed by tmp then
			// right rotate it.
			if (tmp.esq != null) {
				No oldTmp = tmp;
				tmp = tmp.esq;
				oldTmp.esq = tmp.dir;
				if (oldTmp.esq != null)
					oldTmp.esq.pai = oldTmp;
				tmp.dir = oldTmp;
				if (tmp.dir != null)
					tmp.dir.pai = tmp;
				grand.dir = tmp;
				if (grand.dir != null)
					grand.dir.pai = grand;
			}

			// If left dont exists
			// add 1 to count and
			// traverse further right to
			// flatten remaining BST.
			else {
				count++;
				grand = tmp;
				tmp = tmp.dir;
			}
		}

		return count;
	}

	// Function to compress given tree
	// with its root as grand.right.
	private void compress(No grand, int m) {

		// Make tmp pointer to traverse
		// and compress the given BST.
		No tmp = grand.dir;

		// Traverse and left-rotate root m times
		// to compress given vine form of BST.
		for (int i = 0; i < m; i++) {
			No oldTmp = tmp;
			tmp = tmp.dir;
			grand.dir = tmp;
			if (grand.dir != null)
				grand.dir.pai = grand;
			oldTmp.dir = tmp.esq;
			if (oldTmp.dir != null)
				oldTmp.dir.pai = oldTmp;
			tmp.esq = oldTmp;
			if (tmp.esq != null)
				tmp.esq.pai = tmp;
			grand = tmp;
			tmp = tmp.dir;
		}
	}

	// Function to calculate the
	// log base 2 of an integer
	private int log2(int N) {

		// calculate log2 N indirectly
		// using log() method
		return (int) (Math.log(N) / Math.log(2));
	}

	// Function to implement the algorithm
	public void balanceBST() {

		// create an empty dummy node
		No grand = new No("");

		// assign the right of dummy node as our input BST
		grand.dir = raiz;

		// get the number of nodes in input BST and
		// simultaneously convert it into right linked list.
		int count = bstToVine(grand);

		// gets the height of tree in which all levels
		// are completely filled.
		int h = log2(count + 1);

		// get number of nodes until second last level
		int m = (int) Math.pow(2, h) - 1;

		// Left rotate for excess nodes at last level
		compress(grand, count - m);

		// Left rotation till m becomes 0
		// Step is done as mentioned in algo to
		// make BST balanced.
		for (m = m / 2; m > 0; m /= 2) {
			compress(grand, m);
		}

		// return the balanced tree
		raiz = grand.dir;
	}

	@Override
	public String toString() {
		String string = String.format("> N� Elementos: %s\r\n", nElementos);
		if (raiz == null)
			return string;

		No proximo = menor();
		while (proximo != null) {
			string += String.format("> %s (%d): ", proximo.oElemento, proximo.nVezes);
			string += proximo.esq != null ? proximo.esq.oElemento : "null";
			string += " | ";
			string += proximo.dir != null ? proximo.dir.oElemento : "null";
			string += "\r\n";
			proximo = sucessor(proximo);
		}

		return string;
	}
}
