import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
	private static ArvBin arvbin = new ArvBin();
	private static Scanner input = new Scanner(System.in);
	private static final String path = "./bin/textos/";

	private static void inserirPorFicheiro() {
		System.out.print("Insira o nome do ficheiro: ");
		String filename = input.nextLine();

		File file = new File(path + filename);
		if (!filename.isEmpty() && file.exists()) {
			System.out.println("> A ler o ficheiro. Aguarde..");
			String text = readFile(file);

			if (!text.isEmpty()) {
				System.out.println("> A inserir o texto. Aguarde..");
				inserirPalavras(text.toLowerCase());

				System.out.printf("> O texto de '%s' foi inserido com sucesso!\r\n", filename);
			} else
				System.out.printf("> O ficheiro '%s' encontra-se vazio!\r\n", filename);
		} else
			System.out.printf("> O ficheiro '%s' n�o foi encontrado!\r\n", filename);
	}

	private static void inserirPorConsola() {
		System.out.println("Insira abaixo o texto:");
		String text = inputMultiline();

		if (!text.isEmpty()) {
			System.out.println("> A inserir o texto. Aguarde..");
			inserirPalavras(text.toLowerCase());

			System.out.println("> O texto da consola foi inserido com sucesso!");
		} else
			System.out.println("> O texto da consola encontra-se vazio!");
	}

	private static void inserirPalavras(String text) {
		Scanner sc = new Scanner(text);
		while (sc.hasNext())
			arvbin.inserir(sc.next().replaceAll("[.,;/]", "").trim());
		sc.close();
	}

	private static void procurarPalavras() {
		System.out.print("Insira a palavra: ");
		String word = input.nextLine().toLowerCase();

		if (!word.isEmpty()) {
			String list = arvbin.listarProcura(word);
			if (!list.isEmpty()) {
				System.out.print(list);
				return;
			}
			System.out.printf("> N�o foram encontradas palavras come�adas por '%s' na �rvore!\r\n", word);
		} else if (arvbin.estaVazia())
			System.out.println("> A �rvore encontra-se vazia!");
		else
			System.out.print(arvbin.listarOrdem());
	}

	private static void removerPalavras() {
		System.out.print("Insira a palavra: ");
		String word = input.nextLine().toLowerCase();

		if (arvbin.remover(word))
			System.out.printf("> A palavra '%s' foi apagada da �rvore com sucesso!\r\n", word);
		else
			System.out.printf("> A palavra '%s' n�o foi encontrada na �rvore!\r\n", word);
	}

	private static void balancearArvore() {
		System.out.println("> A balancear a �rvore. Aguarde..");
		arvbin.balanceBST();
		System.out.println("> A �rvore foi balanceada com sucesso!");
	}

	private static void printTimers(long s, long e) {
		System.out.println();
		System.out.println("============================================================");
		System.out.printf("> O processo de execu��o do m�todo demorou %dns.\r\n", (e - s));
		System.out.println("============================================================");
	}

	public static void main(String[] args) {
		final String menu = "MENU PRINCIPAL\r\n" + "1. Inserir texto a partir de um ficheiro\r\n"
				+ "2. Inserir texto a partir da consola\r\n" + "3. Procurar palavras\r\n"
				+ "4. Remover palavras\r\n" + "5. Balancear a �rvore\r\n" + "0. Terminar\r\n"
				+ "\r\n" + "Op��o: ";

		long sTime = 0;
		char opcao = '0';
		menu: do {
			System.out.print(menu);
			opcao = (input.nextLine() + (char) 13).charAt(0);
			switch (opcao) {
			case '1':
				inserirPorFicheiro();
				break;
			case '2':
				inserirPorConsola();
				break;
			case '3':
				sTime = System.nanoTime();
				procurarPalavras();
				printTimers(sTime, System.nanoTime());
				break;
			case '4':
				sTime = System.nanoTime();
				removerPalavras();
				printTimers(sTime, System.nanoTime());
				break;
			case '5':
				balancearArvore();
				break;
			case '9':
				System.out.println(arvbin.toString());
				break;
			case '0':
				break menu;
			default:
				System.out.printf("Op��o '%s' n�o reconhecida!\r\n", opcao == 13 ? "ENTER" : opcao);
				break;
			}
			System.out.println();
			input.nextLine();
		} while (opcao != '0');
		input.close();
	}

	private static String inputMultiline() {
		String text = "";

		String line = input.nextLine();
		while (!line.isEmpty()) {
			text += line + "\r\n";
			line = input.nextLine();
		}

		return text;
	}

	private static String readFile(File file) {
		String text = "";

		try {
			Scanner sc = new Scanner(file);
			while (sc.hasNextLine())
				text += sc.nextLine() + "\r\n";
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return text;
	}
}
