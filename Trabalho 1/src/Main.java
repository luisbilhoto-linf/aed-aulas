public class Main {
	// -----------------------------------------------------------------------------
	public static void main(String[] args) {
		Carta baralho[] = criarBaralho();
		Carta j1[] = new Carta[10]; // m�o de cada jogador
		Carta j2[] = new Carta[10];
		Carta j3[] = new Carta[10];
		Carta j4[] = new Carta[10];

		System.out.println("--# BARALHO CRIADO #--\n");
		imprimirCartas(baralho);

		baralho = retirar_8_9_10(baralho);
		System.out.println("\n\n--# BARALHO SEM 8, 9 E 10 #--\n");
		imprimirCartas(baralho);

		embaralhar(baralho);
		System.out.println("\n\n--# BARALHO EMBARALHADO #--\n");
		imprimirCartas(baralho);

		darCartas(baralho, j1, j2, j3, j4);
		System.out.println("\n\n--# BARALHO DISTRIBUIDO PELOS JOGADORES #--\n");
		System.out.println("\n\n --# M�O DO JOGADOR: 1 #--\n");
		imprimirCartas(j1);
		System.out.println("\n\n --# M�O DO JOGADOR: 2 #--\n");
		imprimirCartas(j2);
		System.out.println("\n\n --# M�O DO JOGADOR: 3 #--\n");
		imprimirCartas(j3);
		System.out.println("\n\n --# M�O DO JOGADOR: 4 #--\n");
		imprimirCartas(j4);

		ordenar_jog1(j1);
		ordenar_jog2(j2);
		ordenar_jog3(j3);
		ordenar_jog4(j4);
		System.out.println("\n\nM�O ORDENADA DO JOGADOR: 1 #--\n");
		imprimirCartas(j1);
		System.out.println("\n\nM�O ORDENADA DO JOGADOR: 2 #--\n");
		imprimirCartas(j2);
		System.out.println("\n\nM�O ORDENADA DO JOGADOR: 3 #--\n");
		imprimirCartas(j3);
		System.out.println("\n\nM�O ORDENADA DO JOGADOR: 4 #--\n");
		imprimirCartas(j4);
	}

	// -----------------------------------------------------------------------------
	public static Carta[] criarBaralho() {
		Carta bar[] = new Carta[52];
		int i = 0;
		for (int n = Carta.COPAS; n <= Carta.PAUS; n++)
			for (int f = Carta.AZ; f <= Carta.REI; f++) {
				bar[i] = new Carta(f, n);
				i++;
			}
		return bar;
	}

	// -----------------------------------------------------------------------------
	public static void imprimirCartas(Carta car[]) {
		for (int i = 0; i < car.length; i++)
			System.out.println(car[i]);
	}

	// -----------------------------------------------------------------------------
	public static void embaralhar(Carta bar[]) {
		Carta aux[];
		int a, b, i;

		for (int l = 30; l > 0; l--) {
			aux = bar.clone();
			i = 0;

			// Posi��o da segunda metade do baralho
			a = (int) (Math.random() * (bar.length / 2));

			// Posi��o da segunda parte da segunda metade do baralho
			b = (int) (Math.random() * (bar.length - a)) + a;

			// Adiciona a primeira parte da segunda metade do baralho
			for (int j = a; j < b; j++) {
				bar[i] = aux[j];
				i++;
			}

			// Adiciona a primeira metade do baralho
			for (int j = 0; j < a; j++) {
				bar[i] = aux[j];
				i++;
			}

			// Adiciona a segunda parte da segunda metade do baralho
			for (int j = b; j < 40; j++) {
				bar[i] = aux[j];
				i++;
			}
		}
	}

	// -----------------------------------------------------------------------------
	public static Carta[] retirar_8_9_10(Carta bar[]) {
		Carta nbar[] = new Carta[40];
		int ni = 0;

		// Remove as cartas 8, 9 e 10 diretamente das suas posi��es
		for (int i = 0; i < bar.length; i++) {
			if ((i < 7 || i > 9) && (i < 20 || i > 22) && (i < 33 || i > 35) && (i < 46 || i > 48)) {
				nbar[ni] = bar[i];
				ni++;
			}
		}

		return nbar;
	}

	// -----------------------------------------------------------------------------
	public static void darCartas(Carta bar[], Carta j1[], Carta j2[], Carta j3[], Carta j4[]) {
		for (int i = 0; i < bar.length; i++) {
			switch (i % 4) {
			case 0:
				j1[i / 4] = bar[i];
				break;
			case 1:
				j2[i / 4] = bar[i];
				break;
			case 2:
				j3[i / 4] = bar[i];
				break;
			case 3:
				j4[i / 4] = bar[i];
				break;
			}
		}
	}

	// -----------------------------------------------------------------------------
	public static void ordenar_jog1(Carta j1[]) {

		// Ordena��o pelo algoritmo de sele��o
		for (int i = 0; i < j1.length - 1; i++) {
			int min = i;
			for (int j = i + 1; j < j1.length; j++)
				if (j1[min].compareTo(j1[j]) > j1[j].compareTo(j1[min]))
					min = j;
			Carta aux = j1[min];
			j1[min] = j1[i];
			j1[i] = aux;
		}
	}

	// -----------------------------------------------------------------------------
	public static void ordenar_jog2(Carta j2[]) {

		// Ordena��o pelo algoritmo de inser��o
		for (int i = 1; i < j2.length; i++) {
			Carta x = j2[i];
			int j = i;
			while (j > 0 && j2[j - 1].compareTo(x) > x.compareTo(j2[j - 1])) {
				j2[j] = j2[j - 1];
				j--;
			}
			j2[j] = x;
		}
	}

	// -----------------------------------------------------------------------------
	public static void ordenar_jog3(Carta j3[]) {

		// Ordena��o pelo algoritmo de permuta��o
		boolean ordenado = false;
		int i = 0;
		while (i < j3.length - 1 && !ordenado) {
			ordenado = true;
			for (int j = j3.length - 1; j > i; j--) {
				if (j3[j - 1].compareTo(j3[j]) > j3[j].compareTo(j3[j - 1])) {
					Carta aux = j3[j - 1];
					j3[j - 1] = j3[j];
					j3[j] = aux;
					ordenado = false;
				}
			}
			i++;
		}
	}

	// -----------------------------------------------------------------------------
	public static void ordenar_jog4(Carta j4[]) {

		// Ordena��o pelo algoritmo de shell sort
		int n = j4.length;
		for (int h = n / 2; h > 0; h = h / 2) {
			for (int i = h; i < n; i++) {
				Carta x = j4[i];
				int j = i;
				while (j >= h && j4[j - h].compareTo(x) > x.compareTo(j4[j - h])) {
					j4[j] = j4[j - h];
					j -= h;
				}
				j4[j] = x;
			}
		}
	}

	// -----------------------------------------------------------------------------
}
