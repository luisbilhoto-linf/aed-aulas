public class Lista {
	private int nElementos;
	private No cabeca;

	public Lista() {
//		DECLARADOS EM LIMPAR()
//		cabeca = null;
//		nElementos = 0;
		limpar();
	}

	private class No {
		No(String s) {
			item = s; // o item passa a ser o elemento
			prox = null; // o n� � inicializado com
			// prox a null (n�o h� pr�ximo)
		}

		No prox;
		String item;
	}

	public void inserirOrdem(String s) {
		No novoNo = new No(s);
		No actual = cabeca;
		No anterior = null;
		// procurar o local onde inserir
		while (actual != null && actual.item.compareTo(s) < 0) {
			anterior = actual;
			actual = actual.prox;
		}
		novoNo.prox = actual;
		if (anterior == null)
			cabeca = novoNo;
		else
			anterior.prox = novoNo;
		nElementos++;
	}

	public int procurar(String s) {
		No actual = cabeca;
		int pos = 0;
		while (actual != null && actual.item.compareTo(s) < 0) {
			actual = actual.prox;
			pos++;
		}
		if (actual != null && actual.item.equals(s))
			return pos;
		return -1;
	}

//	public int procura(String s) {
//		return procura(s, 0);
//	}
//
//	public int procura(String s, int posIni) {
//		if (!eIndiceValido(posIni))
//			throw new ArrayIndexOutOfBoundsException(posIni);
//		No actual = cabeca;
//		int pos = 0;
//		while (pos < posIni) { // ir at� posi��o inicial
//			actual = actual.prox;
//			pos++;
//		}
//		while (actual != null && !actual.item.equals(s)) {
//			pos++;
//			actual = actual.prox;
//		}
//		return actual == null ? -1 : pos;
//	}
//
//	public int procuraFim(String s) {
//		return procuraFim(s, nElementos);
//	}
//
//	public int procuraFim(String s, int posFim) {
//		if (!eIndiceValido(posFim))
//			throw new ArrayIndexOutOfBoundsException(posFim);
//		No actual = cabeca;
//		int pos = 0;
//		int ultimaPos = -1;
//		while (actual != null && pos < posFim) {
//			if (actual.item.equals(s))
//				ultimaPos = pos;
//			pos++;
//			actual = actual.prox;
//		}
//		return ultimaPos;
//	}

	public boolean estaPresente(String s) {
		return procurar(s) != -1;
	}

	public int numRepeticoes(String s) {
		No actual = cabeca;
		int nVezes = 0;
		while (actual != null) {
			if (actual.item.equals(s)) {
				nVezes++;
			}
			actual = actual.prox;
		}
		return nVezes;
	}

	public int numSimilares(String s) {
		No actual = cabeca;
		int nVezes = 0;
		while (actual != null) {
			if (actual.item.startsWith(s)) {
				nVezes++;
			}
			actual = actual.prox;
		}
		return nVezes;
	}

	public boolean eIndiceValido(int idx) {
		return idx >= 0 && idx < nElementos;
	}

	public void retirar(int idx) {
		if (!eIndiceValido(idx))
			throw new ArrayIndexOutOfBoundsException(idx);
		No actual = cabeca;
		No anterior = null;
		int pos = 0;
		while (actual != null && pos < idx) {
			anterior = actual;
			actual = actual.prox;
			pos++;
		}
		if (anterior != null)
			anterior.prox = actual.prox;
		else
			cabeca = actual.prox;
		nElementos--;
	}

	public Object get(int idx) {
		if (!eIndiceValido(idx))
			throw new ArrayIndexOutOfBoundsException(idx);
		No actual = cabeca;
		int pos = 0;
		while (pos < idx) {
			pos++;
			actual = actual.prox;
		}
		return actual.item;
	}

	public Object set(String s, int idx) {
		if (!eIndiceValido(idx))
			throw new ArrayIndexOutOfBoundsException(idx);
		No actual = cabeca;
		int pos = 0;
		while (pos < idx) {
			pos++;
			actual = actual.prox;
		}
		Object antigo = actual.item;
		actual.item = s;
		return antigo;
	}

	public int getnElementos() {
		return nElementos;
	}

	public void limpar() {
		cabeca = null;
		nElementos = 0;
	}
}
