import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
	private static String file = "./bin/palavras.txt";
	private static Lista palavras = new Lista();

	public static void main(String[] args) {
		long inicio, fim, duracao;

		int opcao = 0;
		String palavra;
		Scanner input = new Scanner(System.in);

		do {
			System.out.println("MENU PRINCIPAL");
			System.out.println("1. Ler Ficheiro");
			System.out.println("2. Procurar Palavra");
			System.out.println("3. Apagar Palavra");
			System.out.println("4. Gravar Ficheiro");
			System.out.println("5. Terminar");
			System.out.println();
			System.out.print("Opção: ");
			opcao = input.nextInt();
			input.nextLine();
			inicio = System.nanoTime();
			switch (opcao) {
			case 1:
				palavras.limpar();
				System.out.println("> A ler o ficheiro. Aguarde..");
				if (readFile()) {
					System.out.println("> Ficheiro lido com sucesso!");
				}
				break;
			case 2:
				System.out.print("Palavra: ");
				palavra = input.nextLine().toLowerCase();
				System.out.printf("> %d palavras começadas por [%s]\n", palavras.numSimilares(palavra), palavra);
				if (palavras.estaPresente(palavra)) {
					System.out.printf("> [%s] encontra-se na posição %d\n", palavra, palavras.procurar(palavra));
				} else {
					System.out.print("Pretende inserir esta palavra? (S/N) ");
					if (input.nextLine().toUpperCase().charAt(0) == 'S') {
						palavras.inserirOrdem(palavra);
						System.out.printf("> a lista agora possui %d palavra(s)\n", palavras.getnElementos());
					}
				}
				break;
			case 3:
				System.out.print("Palavra: ");
				palavra = input.nextLine().toLowerCase();
				if (palavras.estaPresente(palavra)) {
					palavras.retirar(palavras.procurar(palavra));
					System.out.printf("> A palavra [%s] foi apagada com sucesso!", palavra);
				} else {
					System.out.printf("> A palavra [%s] não foi encontrada!", palavra);
				}
				break;
			case 4:
				System.out.println("> A gravar as palavras no ficheiro. Aguarde..");
				if (writeFile()) {
					System.out.println("> As palavras foram guardadas com sucesso!");
				}
				break;
			default:
				System.out.println("ERRO: Opção inválida!");
				break;
			}
			if (opcao == 1 || opcao == 2 || opcao == 3 || opcao == 4) {
				fim = System.nanoTime();
				duracao = fim - inicio;
				System.out.printf("A operação demorou %d milisegundos.", duracao / 1000000);
			}
			System.out.println();
			input.nextLine();
		} while (opcao != 0);
		input.close();
	}

	public static boolean readFile() {
		File file = new File(Main.file);
		try {
			Scanner sc = new Scanner(file);
			while (sc.hasNextLine()) {
				palavras.inserirOrdem(sc.nextLine());
			}
			sc.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean writeFile() {
		File file = new File(Main.file);
		try {
			PrintWriter pw = new PrintWriter(file);
			for (int i = 0; i < palavras.getnElementos(); i++) {
				pw.println(palavras.get(i));
			}
			pw.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
	}
}
