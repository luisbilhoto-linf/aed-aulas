class Vector {
	private Object buffer[]; // onde se vai armazenar na realidade os elementos
	private int nElems; // n� de elementos �teis no vector
	private int capMaxima; // n� de elementos m�ximo no vector
	private int incremento; // n� de elementos a adicionar quando estiver cheio

	public Vector() {
		this(1, 1); // capacidade inicial 1 e incremento 1
	}

	public Vector(int capInicial) {
		this(capInicial, capInicial / 2 + 1);
// capacidade inicial e incremento metade da capacidade inicial
	}

	public Vector(int capInicial, int incre) {
		if (capInicial <= 0)
			capInicial = 1;
		if (incre <= 0)
			incre = capInicial / 2 + 1;
		nElems = 0;
		buffer = new Object[capInicial];
		capMaxima = capInicial;
		incremento = incre;
	}

	public boolean estaCheio() { // indica se vector est� cheio
		return nElems == capMaxima;
	}

	public void adicionar(Object x) {
		if (estaCheio())
			aumentar();
		buffer[nElems] = x;
		nElems++;
	}

	private void aumentar() {
		Object novoBuffer[] = new Object[capMaxima + incremento];
		for (int i = 0; i < nElems; i++)
			novoBuffer[i] = buffer[i];
		buffer = novoBuffer;
		capMaxima += incremento;
	}

	public void retirar(int idx) {
		buffer[idx] = buffer[nElems - 1];
		nElems--;
	}

	public int procura(Object e, int posIni) {
		for (int i = posIni; i < nElems; i++) {
			if (buffer[i].equals(e)) {
				return i;
			}
		}
		return -1;
	}

	public int procura(Object e) {
		return procura(e, 0);
	}

	public void retirar(Object e) {
		int idx = procura(e);
		if (idx != -1)
			retirar(idx);
	}

	public void retirarTodas(Object e) {
		int idx = procura(e);
		while (idx != -1) {
			retirar(idx);
			idx = procura(e, idx);
		}
	}

	public int procuraFim(Object e, int posFim) {
		for (int i = posFim; i >= 0; i--)
			if (buffer[i].equals(e))
				return i;
		return -1;
	}

	public int procuraFim(Object e) {
		return procuraFim(e, nElems - 1);
	}

	private boolean indiceValido(int idx) {
		return idx < nElems;
	}

	public Object get(int idx) {
		if (!indiceValido(idx))
			throw new ArrayIndexOutOfBoundsException();
		return buffer[idx];
	}

	public void set(int idx, Object e) {
		if (!indiceValido(idx))
			throw new ArrayIndexOutOfBoundsException();
		buffer[idx] = e;
	}

	// -----
	public int getTamanho() {
		return nElems;
	}

	public int getCapacidade() {
		return capMaxima;
	}

	public int getEspacoLivre() {
		return getCapacidade() - getTamanho();
	}

	public void setIncremento(int novoInc) {
		incremento = novoInc <= 0 ? incremento : novoInc;
	}
}