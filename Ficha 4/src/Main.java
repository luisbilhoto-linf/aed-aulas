import java.util.Scanner;
//import java.util.Vector;

//-------------------------------------------------------------------------------------------------
public class Main {
//-------------------------------------------------------------------------------------------------	
//	private static Vector<Integer> v = new Vector<>();
	private static Vector v = new Vector();

//-------------------------------------------------------------------------------------------------	
	public static void main(String[] args) {

		int opcao = 0;
		Scanner input = new Scanner(System.in);

		do {
			imprimirMenu();
			opcao = input.nextInt();
			input.nextLine();
			switch (opcao) {
			case 0:
				break;
			case 1:
				generateElems();
				System.out.println("Foram inseridos 10 n�meros aleat�rios ao vector.");
				System.out.println();
				break;
			case 2:
				System.out.println(printElems());
				break;
			case 3:
				System.out.print("Elemento(s) a apagar: ");
				deleteElems(input.nextInt());
				System.out.println();
				break;
			case 4:
				int[] r = getMinMax();
				System.out.printf("O maior elemento �: %d\n", r[1]);
				System.out.printf("O menor elemento �: %d\n", r[0]);
				System.out.println();
				break;
			default:
				System.out.println("ERRO: Op��o inv�lida!\n");
			}
		} while (opcao != 0);
		input.close();
	}

//-------------------------------------------------------------------------------------------------
	private static void imprimirMenu() {
		System.out.println("1 � Inserir elementos");
		System.out.println("2 � Listar o vector");
		System.out.println("3 � Apagar elementos");
		System.out.println("4 � Encontrar o maior e o menor elemento");
		System.out.println("0 � Terminar\n");
		System.out.println("Op��o => ");
	}

//-------------------------------------------------------------------------------------------------
	private static int novoNumero() {
		return (int) (Math.random() * 1000.0);
	}

//-------------------------------------------------------------------------------------------------	
	private static void generateElems() {
		for (int i = 0; i < 10; i++) {
			v.adicionar(novoNumero());
		}
	}
	
	private static String printElems() {
		String s = "";
		for (int i = 0; i < v.getTamanho(); i++) {
			s += String.format("%s: %s\n", i, v.get(i));
		}
		return s;
	}

	private static void deleteElems(int num) {
		v.retirarTodas(num);
	}

	private static int[] getMinMax() {
		int max = (int)v.get(0), min = (int)v.get(0);
		for (int i = 0; i < v.getTamanho(); i++) {
			max = (int)v.get(i) > max ? (int)v.get(i) : max;
			min = (int)v.get(i) < min ? (int)v.get(i) : min;
		}
		return new int[] { min, max };
	}

////-------------------------------------------------------------------------------------------------	
//	private static void generateElems() {
//		for (int i = 0; i < 10; i++) {
//			v.add(novoNumero());
//		}
//	}
//		
//	private static String printElems() {
//		String s = "";
//		for (int i = 0; i < v.size(); i++) {
//			s += String.format("%s: %s\n", i, v.get(i));
//		}
//		return s;
//	}
//
//	private static void deleteElems(int num) {
//		for (int i = 0; i < v.size(); i++) {
//			if (v.get(i) == num) { v.remove(i); }
//		}
//	}
//
//	private static int[] getMinMax() {
//		int max = v.get(0), min = v.get(0);
//		for (int i = 0; i < v.size(); i++) {
//			max = v.get(i) > max ? v.get(i) : max;
//			min = v.get(i) < min ? v.get(i) : min;
//		}
//		return new int[] {min, max};
//	}
//-------------------------------------------------------------------------------------------------	
}
